FROM ubuntu:17.04
MAINTAINER Kok How, Teh <khteh@weeloy.com>
# Install dependencies
RUN apt-get update -y
RUN apt-get install -y software-properties-common apt-utils curl wget sudo openssh-server apache2 apache2-utils python-software-properties php7.0 php7.0-fpm php7.0-mysql php-mbstring php-gettext libapache2-mod-php7.0 php7.0-mcrypt php7.0-bz2 php7.0-zip php7.0-xml php7.0-curl php7.0-gmp php7.0-json php7.0-opcache ufw sendmail jpegoptim php7.0-xdebug unzip dnsutils logrotate
# Set Global ServerName to Suppress Syntax Warnings
RUN add-apt-repository "deb http://cz.archive.ubuntu.com/ubuntu zesty web universe"
#RUN echo "ServerName dev.weeloy.asia" >> /etc/apache2/apache2.conf
RUN apt-get update -y
RUN phpenmod mcrypt mbstring

# Configure apache
ENV APACHE_RUN_USER=www-data APACHE_RUN_GROUP=www-data APACHE_LOG_DIR=/var/log/apache2 APACHE_PID_FILE=/var/run/apache2.pid APACHE_RUN_DIR=/var/run/apache2 APACHE_LOCK_DIR=/var/lock/apache2 APACHE_LOG_DIR=/var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR /etc/apache2/ssl /etc/PHP_LIB /root/.aws /var/www/html/tmp/twig_cache/ /var/log/xdebug
ADD feasting_asia.crt weeloy_asia.crt /etc/apache2/ssl/
ADD feasting_asia.key.unsecure /etc/apache2/ssl/feasting_asia.key
ADD weeloy_asia.key.unsecure /etc/apache2/ssl/weeloy_asia.key
RUN a2enconf php7.0-fpm
RUN a2enmod proxy_fcgi setenvif rewrite ssl headers proxy_http deflate expires 
#RUN a2ensite dev.weeloy.asia.conf

# Install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/bin/composer

# Install AWS CLI 
RUN curl -sS https://s3.amazonaws.com/aws-cli/awscli-bundle.zip -o awscli-bundle.zip
RUN unzip awscli-bundle.zip
RUN python3 awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

# Install Filebeat
RUN curl -sS -L -O https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-5.3.0-amd64.deb
RUN dpkg -i filebeat-5.3.0-amd64.deb
ADD filebeat.yml /etc/filebeat/filebeat.yml

ADD cronjob.txt /var/spool/cron/crontabs/root
ADD run.sh /usr/local/bin/run.sh
RUN chmod +x /usr/local/bin/run.sh

RUN rm -rf /var/www/html
ADD weeloy.com /var/www/html
RUN rm -rf /var/www/html/.git /var/www/html/conf /var/www/html/.htaccess /var/www/html/.htaccess_dev /var/www/html/.htaccess_prod /var/www/html/tracking/.htaccess /var/www/html/tracking/.htaccess_dev /var/www/html/tracking/.htaccess_prod /etc/httpd/conf.d/vhosts.conf /etc/apache2/sites-available/default-ssl.conf awscli-bundle awscli-bundle.zip

# Copy configuration
ADD weeloy.com/.server/php/php-7.0_prod_bean.ini /etc/php/7.0/apache2/php.ini
ADD weeloy.com/conf /etc/PHP_LIB/conf
ADD weeloy.com/lib /etc/PHP_LIB/lib

#RUN sed -i.bak 's/short_open_tag = Off/short_open_tag = On/' /etc/php/7.0/apache2/php.ini
ADD weeloy.com/conf/conf.mysql_prod_sydney.inc.php /etc/PHP_LIB/conf/conf.mysql.inc.php
ADD weeloy.com/conf/conf.init_prod_sydney.inc.php /etc/PHP_LIB/conf/conf.init.inc.php

ADD weeloy.com/.htaccess_prod_sydney /var/www/html/.htaccess

ADD weeloy.com/tracking/.htaccess_prod_sydney /var/www/html/tracking/.htaccess
ADD weeloy.com/robots_disallow.txt /var/www/html/robots.txt

ADD weeloy.com/.server/httpd/vhosts_sydney.conf /etc/apache2/sites-available/000-default.conf
#mv .server/httpd/httpd_check_service.sh /etc/httpd/httpd_check_service.sh  >> ${LOGFILE} 2>&1
#chmod +x /etc/httpd/httpd_check_service.sh

ADD weeloy.com/.server/aws/config_sydney /root/.aws/config
ADD weeloy.com/.server/aws/credentials_sydney /root/.aws/credentials

RUN chmod -R 777 /var/www/html/tmp

# Install app
#RUN cd /var/www && /usr/bin/composer install
#VOLUME [ "/var/www/vhosts" ]
# Add volume for sessions to allow session persistence
#VOLUME ["/var/www/vhosts", "/sessions", "${MYSQL_DATA_DIR}", "${MYSQL_RUN_DIR}"]
RUN chown -R www-data:www-data /var/www/html /var/log/xdebug
RUN chmod -R g+ws /var/log
RUN grep -rl 'css/style.css"' /var/www/html | xargs sed -i 's/css\/style.css\"/css\/style.css?'$(date +%s)'\"/g'
#RUN aws s3 cp /var/www/html/client/test.php s3://static.weeloy.asia/js/  --region ap-southeast-2 --acl public-read --cache-control 'public, max-age=2592000, s-maxage=2592000'
RUN echo "`hostname -i` www.weeloy.asia" >> /etc/hosts
#RUN service ssh start
#RUN service cron start
#RUN service filebeat start
WORKDIR /var/www

EXPOSE 80 443 8000
#ENTRYPOINT [ "/usr/sbin/apache2" ]
#CMD ["/usr/sbin/apache2", "-D",  "FOREGROUND"]
CMD ["/usr/local/bin/run.sh"]
